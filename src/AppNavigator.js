import React, { Component } from 'react';
import { Container, Content, InputGroup, Text, Input, Button, Icon, Item, Label, Form} from 'native-base';
import {
  AppRegistry,
  StyleSheet,
  Navigator,
  View
} from 'react-native';

import HomeFragment  from './components/home/'
import Login from './components/login/'

export default class AppNavigator extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
      routes: React.PropTypes.array,
    }),
  }

  
  popRoute(){
    this.props.popRoute();
  }

  nextPage(){
    return(
      <HomeFragment/>
    )
  }


  render() {
    return (
      <Navigator
        initialRoute={{id: 'login'}}
        renderScene={this.navigatorRenderScene}/>
    );
  }

  navigatorRenderScene(route, navigator) {
    _navigator = navigator;
    switch (route.id) {
      case 'login':
        return (<Login navigator={navigator} title="login"/>);
      case 'home':
        return (<HomeFragment navigator={navigator} title="home" />);
    }
  }
}