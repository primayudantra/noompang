import React, { Component } from 'react';
import { Container, Content, Left, Body, Right, ListItem, Thumbnail, Text, Icon} from 'native-base';

import {
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';


const avatar = require('../../../images/prima.jpg');
export default class SidebarFragment extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <View>
      	<Text>Sidebar here</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:20,
    flex: 1,
  },
  friend: {
    paddingBottom:20,
  },
  register: {
    fontSize: 14,
  }
});
