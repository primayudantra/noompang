import React, { Component } from 'react';
import { Container, Content, Tab, Button, Text,Icon, Card, CardItem, View} from 'native-base';
import { Col, Grid, Row } from 'react-native-easy-grid';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import ResponsiveImage from 'react-native-responsive-image';


import TabOne from './tabOne'

const avatar = require('../../../images/prima.jpg');


class ProfileFragment extends Component{
  constructor(props){
    super(props);
    this.state = {page:'first'};
  }
  render(){
    return(
      <Content>
        <View>
          <View style={{backgroundColor:'#FFFFFF',padding:10}}>
              <Grid>
                <Col style={{ width: 150 }}>
                  <ResponsiveImage source={avatar} initWidth="150" initHeight="150"/>
                </Col>
                <Col>
                  <Text style={{fontSize:18,paddingTop:5,paddingBottom:5}}>Prima Yudantra</Text>
                  <Text style={{fontSize:14,paddingTop:5,paddingBottom:5}}>Distance Traveled</Text>
                  <Text style={{fontSize:14,paddingTop:5,paddingBottom:5}}>4 Rides | 4 Friends</Text>
                  <Button dark bordered small style={{marginTop:10}}>
                      <Text>Edit Profile</Text>
                  </Button>
                </Col>
              </Grid>
          </View>
          <ScrollableTabView>
            <TabOne tabLabel="Timeline" />
          </ScrollableTabView>
        </View>
      </Content>
    )
  }
}
export default ProfileFragment
