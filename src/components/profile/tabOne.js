import React, { Component } from 'react';
import { Container, Content, Tab, Button, Text,Icon, Card, CardItem, View} from 'native-base';
import { Col, Grid, Row } from 'react-native-easy-grid';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import ResponsiveImage from 'react-native-responsive-image';

import data from './data.js';
const avatar = require('../../../images/prima.jpg');
const chelsea = require('../../../images/chelsea_islan.jpg');
class TabOne extends Component{
  render(){
    let content = data.map(item =>{
      return(
        <CardItem key={item.userId}>
          <Grid>
            <Col>
              <ResponsiveImage source={avatar} initWidth="60" initHeight="60"/>
            </Col>
            <Col style={{width:250}}>
              <Text style={{fontSize:14}}>{item.date} | {item.time} | 1m</Text>
              <Text style={{fontSize:14}}>{item.from} - {item.to}</Text>
              <Text>Notes: Testing aja</Text>
            </Col>
          </Grid>
        </CardItem>
      )
    })
    return(
      <Content>
        <View>
          <Card>
            {content}
          </Card>
        </View>
      </Content>
    )
  }
}
export default TabOne
