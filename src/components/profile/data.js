const data = [
    {
        userId: 1,
        username: 'primayudantra',
        routeId: 12,
        from:'Bandung',
        to:'Jakarta',
        date:'12/4/2017',
        time:'4:43 PM',
        status:'Passenger',
        urlImage:"chelsea"
    },
    {
        userId: 2,
        username: 'primayudantra',
        routeId: 12,
        from:'Jakarta',
        to:'Bandung',
        date:'12/4/2017',
        time:'4:43 PM',
        status:'Passenger',
        urlImage:"avatar"
    }
];

export default data;