import React, { Component } from 'react';
import { Container, Content, InputGroup, Text, Input, Button, Icon, Item, Label, Form} from 'native-base';
import {
  AppRegistry,
  StyleSheet,
  Navigator,
  View
} from 'react-native';

export default class Login extends Component {

  constructor(props){
    super(props);
    this.state = {
      componentSelected:'register'
    };
  }


  nextPage(){
    this.props.navigator.push({
      id: 'home'
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Container>
           <Content>
             <Item stackedLabel>
                 <Label>Username</Label>
                 <Input />
             </Item>
             <Item stackedLabel>
                 <Label>Password</Label>
                 <Input secureTextEntry/>
             </Item>
             <Button 
                style={{marginTop:20}} dark full
                onPress={this.nextPage.bind(this)}>
                  <Text>Login</Text>
              </Button>
           </Content>
        </Container>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:20,
    flex: 1,
    margin:40,
  },
  register: {
    fontSize: 14,
  }
});
