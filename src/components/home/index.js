import React, { Component } from 'react';
import { Container, Content, InputGroup, Text,Drawer, Input, Button, Item, Header,Left,Right,Body,Title,Icon,Label, Form} from 'native-base';
import {
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import DefaultTab from './defaultTab';

import TimelineFragment from '../timeline'
import FriendsFragment from '../friends'
import DriverPassengerFragment from '../form'
import ChatFragment from '../chat'
import ProfileFragment from '../profile'
import SidebarFragment from '../sidebar'


export default class HomeFragment extends Component {
  render() {
    closeDrawer = () => {
      this._drawer._root.close()
    };
    openDrawer = () => {
      this._drawer._root.open()
    };
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor:"#FFFFFF"}}>
            <Left>
                <Button 
                  transparent
                  onIconClicked={() => {this.openDrawer()}}
                >
                    <Icon style={{color:"#000"}} name='menu' />
                </Button>
            </Left>
            <Body>
                <Title style={{color:"#000"}}>Noompang</Title>
            </Body>
            <Right>
              {/*only for empty spaces*/}
            </Right>
        </Header>
        <ScrollableTabView
          tabBarPosition="bottom"
          renderTabBar={() => <DefaultTab />}
          ref={(tabView) => { this.tabView = tabView; }}
        >
          <TimelineFragment tabLabel="ios-home" />
          <FriendsFragment tabLabel="ios-people" />
          <DriverPassengerFragment tabLabel="ios-add-circle" />
          <ChatFragment tabLabel="ios-chatboxes" />
          <ProfileFragment tabLabel="ios-person" />
        </ScrollableTabView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header:{
    backgroundColor:"#FFFFFF",
  },
  register: {
    fontSize: 14,
  }
});
