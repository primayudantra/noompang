import React, { Component } from 'react';
import { Container, Content, Form, Item, Input, Button, Picker} from 'native-base';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import DateTimePicker from 'react-native-modal-datetime-picker'


export default class DriverPassengerFragment extends Component {
  constructor(props){
    super(props);
    this.state = {
      selectedItem: undefined,
      selected1: 'key1',
      results: {
          items: []
      }
    }
  }

  onValueChange (value: string) {
      this.setState({
          selected1 : value
      });
  }
  clickButton(){
    console.log("test")
  }
  render() {
    return (
      <View style={styles.container}>
        <Container>
          <Content>
              <Form>
                  <Item>
                      <Input placeholder="From" />
                  </Item>
                  <Item>
                      <Input placeholder="To" />
                  </Item>
                  <Item>
                      <Input placeholder="Notes" Textbox/>
                  </Item>
                  <Item>
                      <Input placeholder="YYYY/MM/DD" Textbox/>
                  </Item>
                   <Picker
                        iosHeader="Driver / Passenger"
                        androidHeader="Driver / Passenger"
                        mode="dropdown"
                        selectedValue={this.state.selected1}
                        onValueChange={this.onValueChange.bind(this)}>
                        <Item label="Driver" value="key0" />
                        <Item label="Passenger" value="key1" />
                   </Picker>
                  <Button 
                    style={{marginTop:20}} dark full
                    onPress={this.clickButton()}
                    >
                      <Text style={{color:"#FFFFFF"}}>Submit</Text>
                  </Button>
                  {/*
                  <DatePicker
                    style={{ alignSelf: 'stretch',flex:3}}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    onDateChange={(date) => {this.setState({date: date})}}
                    />
                  */}
              </Form>
          </Content>
        </Container>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin:20,
  },
  register: {
    fontSize: 14,
  }
});
