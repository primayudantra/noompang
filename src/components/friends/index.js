import React, { Component } from 'react';
import { Header, Container, Content, Left,Item,Input,Button, Body, Right, ListItem, Thumbnail, Text, Icon} from 'native-base';

import {
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';


const avatar = require('../../../images/prima.jpg');
export default class FriendsFragment extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <Container>
        <Content>
            <ListItem itemDivider>
                <Text>A</Text>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem itemDivider>
                <Text>B</Text>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem itemDivider>
                <Text>C</Text>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward-outline"></Icon>
                </Right>
            </ListItem>
        </Content>
    </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:20,
    flex: 1,
  },
  friend: {
    paddingBottom:20,
  },
  register: {
    fontSize: 14,
  }
});
