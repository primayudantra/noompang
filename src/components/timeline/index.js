import React, { Component } from 'react';
import { Container, Header, Title, Content, Text, Button, Icon, Footer, Fab,FooterTab, Card, CardItem} from 'native-base';
import { View,StyleSheet } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
import ResponsiveImage from 'react-native-responsive-image';


const avatar = require('../../../images/prima.jpg');
const passenger = require('../../../images/icon/passenger.jpg');
const driver = require('../../../images/icon/driver.jpg');

import data from './data.js'
export default class TimelineFragment extends Component {
  render() {
    let content = data.map(item => {
    return(
        <Card key={item.route.routeId}>
          <CardItem>
            <Grid>
              <Col>
                <ResponsiveImage source={avatar} initWidth="60" initHeight="60"/>
              </Col>
              <Col style={{width:200}}>
                <Text style={{fontWeight:'bold'}}>{item.fullName}</Text>
                <Text><Icon name='ios-pin' style={{fontSize:20}} /> {item.route.to} - {item.route.from} </Text>
                <Text><Icon name='ios-calendar-outline' style={{fontSize:20}} /> {item.route.date} </Text>
                <Text><Icon name='ios-clock-outline' style={{fontSize:20}} /> {item.route.time} </Text>
                <Text>{item.route.notes} </Text>
              </Col>
              <Col>
                <ResponsiveImage source={passenger} initWidth="60" initHeight="60"/>
              </Col>
            </Grid>
          </CardItem>
        </Card>
      )
    })
    return (
      <Content>
        <View style={styles.container}>
          {content}
        </View>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  register: {
    fontSize: 14,
  },
   actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});
