const data = [
  {
      userId: 1,
      username: 'primayudantra',
      fullName: 'Prima Yudantra',
      route:{
        routeId: 12,
        from:'Bandung',
        to:'Jakarta',
        date:'12/4/2017',
        time:'4:43 PM',
        notes: "Beliin gw bensin aja jon",
        status:'driver'
      }
  },
  {
      userId: 2,
      username: 'mirsasrim',
      fullName: 'Mirsa Sadikin',
      route:{
        routeId: 13,
        from:'Jakarta',
        to:'Bandung',
        date:'12/4/2017',
        time:'4:43 PM',
        notes: "Noompang ya guys",
        status:'passenger'
      }
  },
  {
      userId: 3,
      username: 'mirsasrim',
      fullName: 'Mirsa Sadikin',
      route:{
        routeId: 14,
        from:'Jakarta',
        to:'Bandung',
        date:'12/4/2017',
        time:'4:43 PM',
        notes: "Noompang ya guys",
        status:'passenger'
      }
  },
];

export default data;