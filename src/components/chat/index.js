import React, { Component } from 'react';
import { Container, Content, Left, Body, Right, ListItem, Thumbnail, Text } from 'native-base';

import {
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';


const avatar = require('../../../images/prima.jpg');
const chelsea = require('../../../images/chelsea_islan.jpg');
export default class ChatFragment extends Component {
  render() {
    return (
      <Container>
        <Content>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                    <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                    <Text note>18.00 PM</Text>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={avatar} />
                </Left>
                <Body>
                    <Text>Prima Yudantra</Text>
                    <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                    <Text note>18.00 PM</Text>
                </Right>
            </ListItem>
            <ListItem avatar>
                <Left>
                    <Thumbnail source={chelsea} />
                </Left>
                <Body>
                    <Text>Chelsea Islan</Text>
                    <Text note>Do what you love darling.. </Text>
                </Body>
                <Right>
                    <Text note>18.00 PM</Text>
                </Right>
            </ListItem>
        </Content>
    </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:20,
    flex: 1,
  },
  register: {
    fontSize: 14,
  }
});
